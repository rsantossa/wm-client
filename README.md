# WM Client
This is a SPA created for an test.

### Instructions

1- Clone the repository
```sh
git clone --depth=1 https://bitbucket.org/rsantossa/wm-client
// noticed the depth flag which will make a lean and shallow clone but you'll also might have limited permission in this git repo
```

2 - Then install the following tools (if ain't installed) or go to step 3:
NPM, GULP, LESS, BOWER, PROTRACTOR.


2.1 - Install Node (currently using version v0.10.33)


```sh
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs
```

2.2 - Install these tools
```sh
sudo npm install -g npm
sudo npm install -g gulp -g less -g bower
npm install --save-dev gulp
```

3 - then install dependencies.
```sh
npm install
bower install
```

4 - then run the server:
```sh
gulp
```

5 - To run unit tests, follow steps below:
```sh
sudo npm install -g karma -g karma-cli -g requirejs
// go to root of project and type
karma
```

6 - To run end-to-end tests, follow steps below:
```sh
sudo npm install -g protractor
sudo apt-get install openjdk-7-jre
sudo webdriver-manager update
webdriver-manager start
cd app/scripts/e2e-tests

// to run all tests
protractor config.js

// or to run a specific test suite
protractor config.js --suite addresses
```

and wait for the your browser to load up
or jump to `http://localhost:1337` 

## Technologies

* AngularJS - As SPA framework
* Node - As server for the app and tools
* Npm - As package manager for Node stuff
* Gulp - As task runner
* Bower - As package manager for FrontEnd stuff
* Jasmine - As BDD language
* Karma - As unit test runner
* Protractor - As end-to-end test runner

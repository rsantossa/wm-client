var gulp = require('gulp'),
    debug = require('gulp-debug'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    mainBowerFiles = require('main-bower-files'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minifyCSS = require('gulp-minify-css'),
    clean = require('gulp-clean'),
    plumber = require('gulp-plumber'),
    webserver = require('gulp-webserver');

var config = {
    CSS_SOURCE:'app/styles/src',
    CSS_DIST:'app/styles/dist',
    JS_SOURCE: 'app/scripts/src',
    JS_DIST: 'app/scripts/dist',

    // TODO: CREATE PACK OF JS / CSS FILES AS USED IN OLX
};

gulp.task('moveBowerFiles', function() {
    return gulp.src(mainBowerFiles(), {
            base: 'app/bower_components'
        })
        .pipe(debug())
        .pipe(gulp.dest('app/lib'));
});

gulp.task('cleanStyles', function() {
    return gulp.src(config.CSS_DIST, {
            read: false
        })
        .pipe(clean());
});

gulp.task('cleanScripts', function() {
    return gulp.src(config.JS_DIST, {
            read: false
        })
        .pipe(clean());
});

gulp.task('buildScripts', function() {
    return gulp.src(config.JS_SOURCE + '/*.js')
        .pipe(concat('app.js'))
        // .pipe(uglify()) // add it to a -prod task
        .pipe(gulp.dest(config.JS_DIST))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('buildStyles', function() {
    return gulp.src(config.CSS_SOURCE + '/styles.less')
        .pipe(plumber())
        .pipe(less())
        .pipe(concat('style.css'))
        // .pipe(minifyCSS()) // add it to a -prod task
        .pipe(gulp.dest(config.CSS_DIST))
        .pipe(debug())
        .pipe(reload({
            stream: true
        }));
});

gulp.task('watch', function() {
    gulp.watch([config.JS_SOURCE + '/*.js'], ['buildScripts']);
    gulp.watch([config.CSS_SOURCE + '/*.less'], ['buildStyles']);

    browserSync({
        server: {
            baseDir: './app'
        },
        port: 1337
    });
});

gulp.task('webserver', function() {
    gulp.src('app')
        .pipe(plumber())
        .pipe(webserver({
            host: '0.0.0.0',
            port: 1337,
            livereload: false,
            directoryListing: false,
            fallback: 'app/index.html'
        }));
});

gulp.task('default', [
    'cleanStyles',
    'buildStyles',
    'cleanScripts',
    'buildScripts',
    'watch'
], function() {});

module.exports = function(config) {
    config.set({
        basePath: 'app/',

        files: [
            'lib/requirejs/require.js',
            'lib/angular/angular.js',
            'lib/angular-mocks/angular-mocks.js',
            'scripts/src/*.js',
            'scripts/unit-tests/*.js'
        ],

        autoWatch: true,

        frameworks: ['jasmine'],

        browsers: ['Chrome', 'PhantomJS'], 

        plugins: [
            'karma-jasmine',
            'karma-phantomjs-launcher',
            'karma-chrome-launcher'
        ]

    })
}

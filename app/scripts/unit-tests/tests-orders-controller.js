"use strict";

describe("Test: ordersController", function() {
    var scope, factory, $location;

    beforeEach(function() {
        var mockOrdersService = {};

        module("wmApp", function($provide) {
            $provide.value("mockOrdersService", mockOrdersService);
        });

        inject(function($q) {
            mockOrdersService.data = [{
                id: 137345123,
                status: "Entregue"
            }, {
                id: 175675674,
                status: "Entregue"
            }, {
                id: 262424234,
                status: "Em transporte"
            }, {
                id: 32342343,
                status: "Aguardando pagamento"
            }];

            mockOrdersService.getAllByUserId = function(userId) {
                var defer = $q.defer();

                defer.resolve(this.data);

                return defer.promise;
            };
        });

        beforeEach(inject(function($controller, $rootScope, _$location_, _mockOrdersService_) {
            scope = $rootScope.$new();
            $location = _$location_;
            mockOrdersService = _mockOrdersService_;

            $controller("ordersController", {
                $scope: scope,
                $location: $location,
                ordersService: mockOrdersService
            });

            scope.$digest();
        }));
    });

    it("should contain all orders from a user", function() {
        expect(scope.orders).toEqual([{
            id: 137345123,
                status: "Entregue"
            }, {
                id: 175675674,
                status: "Entregue"
            }, {
                id: 262424234,
                status: "Em transporte"
            }, {
                id: 32342343,
                status: "Aguardando pagamento"
            }
        ]);
    });
});

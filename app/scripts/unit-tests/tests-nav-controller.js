"use strict";

describe("Test: navController", function() {
    var scope, $location, createController;

    beforeEach(inject(function($rootScope, $controller, _$location_) {
        $location = _$location_;
        scope = $rootScope.$new();

        createController = function() {
            return $controller("navController", {
                "$scope": scope
            });
        };
    }));

    it("should have a method to check if the path is active", function() {
        var controller = createController();
        $location.path("/meus-pedidos");

        expect($location.path()).toBe("/meus-pedidos");
        expect(scope.isActive("orders")).toBe(true);
        expect(scope.isActive("addresses")).toBe(false);
        expect(scope.isActive("profile")).toBe(false);
        expect(scope.isActive("")).toBe(false);
    });
});

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
        'browserName': 'chrome'
    },

    onPrepare: function() {
        browser.driver.manage().window().setSize(1200, 800);
    },

    jasmineNodeOpts: {
        showColors: true
    },

    baseUrl: 'http://localhost:1337',

    suites: {
        common: 'common/*.spec.js',
        addresses: 'addresses/*.spec.js',
        orders: 'orders/*.spec.js',
        profile: 'profile/*.spec.js'
    },
};

var OrdersPage = require('./orders.po.js');

describe('Orders Page', function() {
    var page;

    beforeEach(function() {
        page = new OrdersPage();
        page.load();
    });

    it('should enable user to see a list of his orders', function() {
        expect(page.title.getText()).toEqual('Seus Pedidos');
        expect(page.ordersList.isDisplayed()).toBeTruthy();
    });

    it('should enable user to see details of an order', function() {
        expect(page.ordersList.isDisplayed()).toBeTruthy();
    });

    it('should enable user to zoom a picture of product', function() {
        
    });
});
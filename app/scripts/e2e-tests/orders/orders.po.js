var OrdersPage = function() {
    
    this.load = function() {
        browser.get('http://localhost:1337/#meus-pedidos');
    };

    this.ordersList = element(by.id('orders-list'));
    this.title = element(by.id('page-title'));
};

module.exports = OrdersPage;
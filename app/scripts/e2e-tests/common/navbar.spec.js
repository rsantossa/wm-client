var CommonElements = require('./common.po.js');

describe('Main Navbar Navigation', function() {
    var commonElements,
        navItems;

    beforeEach(function() {
        browser.get('');
        commonElements = new CommonElements();
        navItems = commonElements.navItems;
    });

    function testNavClick(pageTitle, index) {
        navItems.get(index).click();

        var linkText = navItems.get(index).getText(),
            pageTitle = element(by.id('pageTitle')).getText();

        expect(linkText).toEqual(pageTitle);
    }

    function testNavHighlight(index) {
        navItems.get(index).click();
        
        expect(navItems.get(index).getAttribute('class')).toMatch('active');
    }

    it('should enable navigation between views', function() {
        testNavClick('Seus Pedidos', 0);
        testNavClick('Seus Endereços', 1);
        testNavClick('Seus Dados', 2);
    });

    it('should highlight menuitem after user clicks', function() {
        testNavHighlight(0);
        testNavHighlight(1);
        testNavHighlight(2);
    });
});
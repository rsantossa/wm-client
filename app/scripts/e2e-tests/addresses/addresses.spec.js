var AddressesPage = require('./addresses.po.js');

describe('Addresses Page', function() {
    var page;

    beforeEach(function() {
        page = new AddressesPage();
        page.load();
    });

    it('should show a list of user\'s addresses', function() {

    });

    it('should enable user to add an address', function() {
        
    });

    it('should enable user to edit an address', function() {
        
    });

    it('should enable user to delete an address', function() {
        
    });
});
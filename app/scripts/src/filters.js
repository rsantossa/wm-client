app.filter('fromNow', function() {
    moment.locale('pt-br');
    return function(date) {
        return moment(date).fromNow();
    }
})

app.filter('setDecimal', function ($filter) {
    return function (input, places) {
        if (isNaN(input)) {
            return input;
        }
        
        var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
        return Math.round(input * factor) / factor;
    };
})
angular.module("wmApp.controllers", [])

.controller("navController", function($scope, $route) {
    $scope.isActive = function(name) {
        return name === $route.current.$$route.name;
    };
})

.controller("pageController", function($scope, $route) {

})

.controller("ordersController", function($scope, $route, ordersService, addressesService) {
    $('[data-toggle="tooltip"]').tooltip();

    $scope.orders = ordersService.getOrdersByUserId(1);
    $scope.visibleOrder = 0;

    $scope.showDetails = function(orderId) {
        var details = ordersService.getOrderDetailsById(orderId);
        $scope.orderDetails = details;
        $scope.visibleOrder = orderId;
        $scope.shippingInfo = addressesService.getAddressById(details.shipping.addressId);
    };
})

.controller("addressesController", function($scope, $route, addressesService) {
    $scope.addresses = addressesService.getAddressesByUserId(1);
    $scope.selectedAddressId = null;
    $scope.address = {};

    $scope.addAddressModal = function(userId) {
        $("#add-address").modal('show');
        $scope.selectedAddressId = 0;
    }

    $scope.editAddressModal = function(addressId) {
        $scope.selectedAddressId = addressId;
        $("#edit-address").modal('show');
    }

    $scope.deleteAddressModal = function(addressId) {
        $scope.selectedAddressId = addressId;
        $("#delete-address").modal('show');
    }

    $scope.deleteAddress = function() {
        var addressId = $scope.selectedAddressId;
        
        $scope.selectedAddressId = 0;
        addressesService.removeAddress(addressId);

        $("#delete-address").modal('hide');
        setTimeout(function() {
            $("#address-" + addressId).fadeOut();
        }, 500);
    }

    $scope.saveAddress = function() {
        addressesService
            .addAddress($scope.address)
            .done(function() {
                // $scope.address = null;
                $scope.addresses = addressesService.getAddressesByUserId(1);
            });

        $($scope.selectedAddressId == 0 ? "#add-address" : "#edit-address").modal('hide');

        $("#action-confirmation").modal('show');
        setTimeout(function() {
            $("#action-confirmation").modal('hide');
        }, 2000);
    }

})

.controller("profileController", function($scope, $route) {
    
});
/**** ORDERS FACTORY ****/
app.factory("ordersService", function($http) {
    return {
        getOrdersByUserId: function(userId) {
            // return $http.get("/www.rafael-santos.com/wm-server/orders/get-all", { 
            //     userId: userId
            // }); 
            return _.where(window.fakeOrders, { userId: userId });
        },

        getOrderDetailsById: function(orderId) {
            var data = _.where(window.fakeOrders, { id: orderId });
            return data[0];
        }
    }
});

/**** ADDRESSES FACTORY ****/
app.factory("addressesService", function() {
    return {
        getAddressesByUserId: function(userId) {
            return _.where(window.fakeAddresses, { userId: userId })
        },

        getAddressById: function(addressId) {
            var data = _.where(window.fakeAddresses, { id: addressId });
            return data[0];
        },

        removeAddress: function(addressId) {
            window.fakeAddresses = _.without(window.fakeAddresses, 
                            _.findWhere(window.fakeAddresses, { id: addressId })
                        );
        },

        addAddress: function(addressObject) {
            var deferred = $.Deferred();
            addressObject.id = window.fakeAddresses.length + 1;
            addressObject.userId = 1;

            window.fakeAddresses.push(addressObject);
            
            deferred.resolve(addressObject);

            return deferred.promise();
        }
    }
});

/**** PROFILE FACTORY ****/
app.factory("usersService", function() {
    return {
        getUserById: function(userId) {
            var data = _.where(window.fakeUser, { id: addressId });
            return data[0];
        }
    }
});
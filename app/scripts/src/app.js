"use strict";

var app = angular.module("wmApp", [
    "ngRoute",
    "ngAnimate",
    "wmApp.controllers"
]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when("/", {
            name: "intro",
            templateUrl: "views/intro.html",
            controller: "pageController"
        })
        .when("/meus-pedidos", {
            name: "orders",
            templateUrl: "views/orders.html",
            controller: "ordersController"
        })
        .when("/meus-enderecos", {
            name: "addresses",
            templateUrl: "views/addresses.html",
            controller: "addressesController"
        })
        .when("/meus-dados", {
            name: "profile",
            templateUrl: "views/profile.html",
            controller: "profileController"
        })
        .otherwise({
            redirectTo: "/"
        });

    $locationProvider.html5Mode(true);
});
window.fakeAddresses = [{
    'userId': 1,
    'id': 1,
    'street': 'Rua Jairo de Almeida Machado',
    'number': '100',
    'complement': 'Casa, 111',
    'zipcode': '02998-060',
    'tag': 'Residência',
    'city': 'São Paulo',
    'state': 'SP'
},
{
    'userId': 1,
    'id': 2,
    'street': 'Rua Gomes de Carvalho',
    'number': '1510',
    'complement': '',
    'zipcode': '04547-005',
    'tag': 'Trabalho',
    'city': 'São Paulo',
    'state': 'SP'
}];

window.fakeOrders = [{
    userId: 1,
    id: 137345123,
    updateDate: 'Jan, 3, 2014',
    status: "Entregue",
    total: 129.95,
    items: [
        { 
            code: '113266753',
            sku: 'RW150-DIR803',
            title: 'Roteador D-LINK DIR-803 750MBPS 11AC Dual BAND',
            thumb: 'http://static.wmobjects.com.br/imgres/arquivos/ids/3835038-55-55/roteador-d-link-dir-803-750mbps-11ac-dual-band.jpg',
            shortDescription: 'DIR-803 é um Roteador Ideal para Casas de médio a grande porte, entre 100 a 150m²',
            unitPrice: 129.95,
            totalPrice: 129.95,
            qty: 1
        }
    ],
    payment: {
        type: 'VISA',
        value: 129.95
    },
    shipping: {
        addressId: 1
    }
}, {
    id: 175675674,
    userId: 1,
    updateDate: 'Dec, 20, 2014',
    status: "Entregue",
    total: 838.65,
    items: [
        { 
            code: '113266753',
            sku: 'RW150-DIR803',
            title: 'Roteador D-LINK DIR-803 750MBPS 11AC Dual BAND',
            thumb: 'http://static.wmobjects.com.br/imgres/arquivos/ids/3835038-55-55/roteador-d-link-dir-803-750mbps-11ac-dual-band.jpg',
            shortDescription: 'DIR-803 é um Roteador Ideal para Casas de médio a grande porte, entre 100 a 150m²',
            unitPrice: 699.70,
            totalPrice: 699.70,
            qty: 1
        },
        { 
            code: '113266753',
            sku: 'RW150-DIR803',
            title: 'Roteador D-LINK DIR-803 750MBPS 11AC Dual BAND',
            thumb: 'http://static.wmobjects.com.br/imgres/arquivos/ids/3835038-55-55/roteador-d-link-dir-803-750mbps-11ac-dual-band.jpg',
            shortDescription: 'DIR-803 é um Roteador Ideal para Casas de médio a grande porte, entre 100 a 150m²',
            unitPrice: 129.95,
            totalPrice: 129.95,
            qty: 1
        }
    ],
    payment: {
        type: 'MasterCard - 6x de R$139,60',
        value: 838.65
    },
    shipping: {
        addressId: 1
    }
}, {
    id: 262424234,
    userId: 1,
    updateDate: 'Feb, 1, 2015',
    status: "Em transporte",
    total: 499.99,
    items: [
        { 
            code: '113266753',
            sku: 'RW150-DIR803',
            title: 'Roteador D-LINK DIR-803 750MBPS 11AC Dual BAND',
            thumb: 'http://static.wmobjects.com.br/imgres/arquivos/ids/3835038-55-55/roteador-d-link-dir-803-750mbps-11ac-dual-band.jpg',
            shortDescription: 'DIR-803 é um Roteador Ideal para Casas de médio a grande porte, entre 100 a 150m²',
            price: 249.99,
            qty: 2
        }
    ],
    payment: {
        type: 'MasterCard',
        value: 499.99
    },
    shipping: {
        addressId: 2
    }
}, {
    id: 32342343,
    userId: 1,
    updateDate: 'Feb, 5, 2015',
    status: "Aguardando pagamento",
    total: 45.99,
    items: [
        { 
            code: '113266753',
            sku: 'RW150-DIR803',
            title: 'Roteador D-LINK DIR-803 750MBPS 11AC Dual BAND',
            thumb: 'http://static.wmobjects.com.br/imgres/arquivos/ids/3835038-55-55/roteador-d-link-dir-803-750mbps-11ac-dual-band.jpg',
            shortDescription: 'DIR-803 é um Roteador Ideal para Casas de médio a grande porte, entre 100 a 150m²',
            price: 49.99,
            qty: 1
        }
    ],
    payment: {
        type: 'Boleto Bancário (desconto de 10%)',
        value: 44.99
    },
    shipping: {
        addressId: 2
    }
}];
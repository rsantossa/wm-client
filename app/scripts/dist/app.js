window.fakeAddresses = [{
    'userId': 1,
    'id': 1,
    'street': 'Rua Jairo de Almeida Machado',
    'number': '100',
    'complement': 'Casa, 111',
    'zipcode': '02998-060',
    'tag': 'Residência',
    'city': 'São Paulo',
    'state': 'SP'
},
{
    'userId': 1,
    'id': 2,
    'street': 'Rua Gomes de Carvalho',
    'number': '1510',
    'complement': '',
    'zipcode': '04547-005',
    'tag': 'Trabalho',
    'city': 'São Paulo',
    'state': 'SP'
}];

window.fakeOrders = [{
    userId: 1,
    id: 137345123,
    updateDate: 'Jan, 3, 2014',
    status: "Entregue",
    total: 129.95,
    items: [
        { 
            code: '113266753',
            sku: 'RW150-DIR803',
            title: 'Roteador D-LINK DIR-803 750MBPS 11AC Dual BAND',
            thumb: 'http://static.wmobjects.com.br/imgres/arquivos/ids/3835038-55-55/roteador-d-link-dir-803-750mbps-11ac-dual-band.jpg',
            shortDescription: 'DIR-803 é um Roteador Ideal para Casas de médio a grande porte, entre 100 a 150m²',
            unitPrice: 129.95,
            totalPrice: 129.95,
            qty: 1
        }
    ],
    payment: {
        type: 'VISA',
        value: 129.95
    },
    shipping: {
        addressId: 1
    }
}, {
    id: 175675674,
    userId: 1,
    updateDate: 'Dec, 20, 2014',
    status: "Entregue",
    total: 838.65,
    items: [
        { 
            code: '113266753',
            sku: 'RW150-DIR803',
            title: 'Roteador D-LINK DIR-803 750MBPS 11AC Dual BAND',
            thumb: 'http://static.wmobjects.com.br/imgres/arquivos/ids/3835038-55-55/roteador-d-link-dir-803-750mbps-11ac-dual-band.jpg',
            shortDescription: 'DIR-803 é um Roteador Ideal para Casas de médio a grande porte, entre 100 a 150m²',
            unitPrice: 699.70,
            totalPrice: 699.70,
            qty: 1
        },
        { 
            code: '113266753',
            sku: 'RW150-DIR803',
            title: 'Roteador D-LINK DIR-803 750MBPS 11AC Dual BAND',
            thumb: 'http://static.wmobjects.com.br/imgres/arquivos/ids/3835038-55-55/roteador-d-link-dir-803-750mbps-11ac-dual-band.jpg',
            shortDescription: 'DIR-803 é um Roteador Ideal para Casas de médio a grande porte, entre 100 a 150m²',
            unitPrice: 129.95,
            totalPrice: 129.95,
            qty: 1
        }
    ],
    payment: {
        type: 'MasterCard - 6x de R$139,60',
        value: 838.65
    },
    shipping: {
        addressId: 1
    }
}, {
    id: 262424234,
    userId: 1,
    updateDate: 'Feb, 1, 2015',
    status: "Em transporte",
    total: 499.99,
    items: [
        { 
            code: '113266753',
            sku: 'RW150-DIR803',
            title: 'Roteador D-LINK DIR-803 750MBPS 11AC Dual BAND',
            thumb: 'http://static.wmobjects.com.br/imgres/arquivos/ids/3835038-55-55/roteador-d-link-dir-803-750mbps-11ac-dual-band.jpg',
            shortDescription: 'DIR-803 é um Roteador Ideal para Casas de médio a grande porte, entre 100 a 150m²',
            price: 249.99,
            qty: 2
        }
    ],
    payment: {
        type: 'MasterCard',
        value: 499.99
    },
    shipping: {
        addressId: 2
    }
}, {
    id: 32342343,
    userId: 1,
    updateDate: 'Feb, 5, 2015',
    status: "Aguardando pagamento",
    total: 45.99,
    items: [
        { 
            code: '113266753',
            sku: 'RW150-DIR803',
            title: 'Roteador D-LINK DIR-803 750MBPS 11AC Dual BAND',
            thumb: 'http://static.wmobjects.com.br/imgres/arquivos/ids/3835038-55-55/roteador-d-link-dir-803-750mbps-11ac-dual-band.jpg',
            shortDescription: 'DIR-803 é um Roteador Ideal para Casas de médio a grande porte, entre 100 a 150m²',
            price: 49.99,
            qty: 1
        }
    ],
    payment: {
        type: 'Boleto Bancário (desconto de 10%)',
        value: 44.99
    },
    shipping: {
        addressId: 2
    }
}];
window.fakeUser = {
    name: 'Rafael Santos Sá'
}
"use strict";

var app = angular.module("wmApp", [
    "ngRoute",
    "ngAnimate",
    "wmApp.controllers"
]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when("/", {
            name: "intro",
            templateUrl: "views/intro.html",
            controller: "pageController"
        })
        .when("/meus-pedidos", {
            name: "orders",
            templateUrl: "views/orders.html",
            controller: "ordersController"
        })
        .when("/meus-enderecos", {
            name: "addresses",
            templateUrl: "views/addresses.html",
            controller: "addressesController"
        })
        .when("/meus-dados", {
            name: "profile",
            templateUrl: "views/profile.html",
            controller: "profileController"
        })
        .otherwise({
            redirectTo: "/"
        });

    $locationProvider.html5Mode(true);
});
angular.module("wmApp.controllers", [])

.controller("navController", function($scope, $route) {
    $scope.isActive = function(name) {
        return name === $route.current.$$route.name;
    };
})

.controller("pageController", function($scope, $route) {

})

.controller("ordersController", function($scope, $route, ordersService, addressesService) {
    $('[data-toggle="tooltip"]').tooltip();

    $scope.orders = ordersService.getOrdersByUserId(1);
    $scope.visibleOrder = 0;

    $scope.showDetails = function(orderId) {
        var details = ordersService.getOrderDetailsById(orderId);
        $scope.orderDetails = details;
        $scope.visibleOrder = orderId;
        $scope.shippingInfo = addressesService.getAddressById(details.shipping.addressId);
    };
})

.controller("addressesController", function($scope, $route, addressesService) {
    $scope.addresses = addressesService.getAddressesByUserId(1);
    $scope.selectedAddressId = null;
    $scope.address = {};

    $scope.addAddressModal = function(userId) {
        $("#add-address").modal('show');
        $scope.selectedAddressId = 0;
    }

    $scope.editAddressModal = function(addressId) {
        $scope.selectedAddressId = addressId;
        $("#edit-address").modal('show');
    }

    $scope.deleteAddressModal = function(addressId) {
        $scope.selectedAddressId = addressId;
        $("#delete-address").modal('show');
    }

    $scope.deleteAddress = function() {
        var addressId = $scope.selectedAddressId;
        
        $scope.selectedAddressId = 0;
        addressesService.removeAddress(addressId);

        $("#delete-address").modal('hide');
        setTimeout(function() {
            $("#address-" + addressId).fadeOut();
        }, 500);
    }

    $scope.saveAddress = function() {
        addressesService
            .addAddress($scope.address)
            .done(function() {
                // $scope.address = null;
                $scope.addresses = addressesService.getAddressesByUserId(1);
            });

        $($scope.selectedAddressId == 0 ? "#add-address" : "#edit-address").modal('hide');

        $("#action-confirmation").modal('show');
        setTimeout(function() {
            $("#action-confirmation").modal('hide');
        }, 2000);
    }

})

.controller("profileController", function($scope, $route) {
    
});

app.filter('fromNow', function() {
    moment.locale('pt-br');
    return function(date) {
        return moment(date).fromNow();
    }
})

app.filter('setDecimal', function ($filter) {
    return function (input, places) {
        if (isNaN(input)) {
            return input;
        }
        
        var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
        return Math.round(input * factor) / factor;
    };
})
/**** ORDERS FACTORY ****/
app.factory("ordersService", function($http) {
    return {
        getOrdersByUserId: function(userId) {
            // return $http.get("/www.rafael-santos.com/wm-server/orders/get-all", { 
            //     userId: userId
            // }); 
            return _.where(window.fakeOrders, { userId: userId });
        },

        getOrderDetailsById: function(orderId) {
            var data = _.where(window.fakeOrders, { id: orderId });
            return data[0];
        }
    }
});

/**** ADDRESSES FACTORY ****/
app.factory("addressesService", function() {
    return {
        getAddressesByUserId: function(userId) {
            return _.where(window.fakeAddresses, { userId: userId })
        },

        getAddressById: function(addressId) {
            var data = _.where(window.fakeAddresses, { id: addressId });
            return data[0];
        },

        removeAddress: function(addressId) {
            window.fakeAddresses = _.without(window.fakeAddresses, 
                            _.findWhere(window.fakeAddresses, { id: addressId })
                        );
        },

        addAddress: function(addressObject) {
            var deferred = $.Deferred();
            addressObject.id = window.fakeAddresses.length + 1;
            addressObject.userId = 1;

            window.fakeAddresses.push(addressObject);
            
            deferred.resolve(addressObject);

            return deferred.promise();
        }
    }
});

/**** PROFILE FACTORY ****/
app.factory("usersService", function() {
    return {
        getUserById: function(userId) {
            var data = _.where(window.fakeUser, { id: addressId });
            return data[0];
        }
    }
});
moment.defineLocale('pt-br', {
    months : 'janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro'.split('_'),
    monthsShort : 'jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez'.split('_'),
    weekdays : 'domingo_segunda-feira_terça-feira_quarta-feira_quinta-feira_sexta-feira_sábado'.split('_'),
    weekdaysShort : 'dom_seg_ter_qua_qui_sex_sáb'.split('_'),
    weekdaysMin : 'dom_2ª_3ª_4ª_5ª_6ª_sáb'.split('_'),
    longDateFormat : {
        LT : 'HH:mm',
        LTS : 'LT:ss',
        L : 'DD/MM/YYYY',
        LL : 'D [de] MMMM [de] YYYY',
        LLL : 'D [de] MMMM [de] YYYY [às] LT',
        LLLL : 'dddd, D [de] MMMM [de] YYYY [às] LT'
    },
    calendar : {
        sameDay: '[Hoje às] LT',
        nextDay: '[Amanhã às] LT',
        nextWeek: 'dddd [às] LT',
        lastDay: '[Ontem às] LT',
        lastWeek: function () {
            return (this.day() === 0 || this.day() === 6) ?
                '[Último] dddd [às] LT' : // Saturday + Sunday
                '[Última] dddd [às] LT'; // Monday - Friday
        },
        sameElse: 'L'
    },
    relativeTime : {
        future : 'em %s',
        past : '%s atrás',
        s : 'segundos',
        m : 'um minuto',
        mm : '%d minutos',
        h : 'uma hora',
        hh : '%d horas',
        d : 'um dia',
        dd : '%d dias',
        M : 'um mês',
        MM : '%d meses',
        y : 'um ano',
        yy : '%d anos'
    },
    ordinalParse: /\d{1,2}º/,
    ordinal : '%dº'
});